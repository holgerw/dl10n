2021-03-30  Laura Arjona Reina <larjona@debian.org>

    * Add support for Persian language

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/gen-material, cron/spiderbts, cron/spiderinit: Fix bashisms.

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/gen-material: Use dl10n.conf instead of gen-material.cfg
	* etc/gen-material.cfg: Removed
	* cron/gen-material: Remove generation of po and podebconf tar
	balls. If needed we can ask DSA to have an rsynch'able directory.

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/spiderinit: Do not overwrite PATH.

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/spiderbts: Use dl10n.conf
	* cron/spiderbts: Cleanups.
	* cron/spiderbts: Update to the i18n.d.o layout.
	* cron/spiderbts: Do not create the html directories, dl10n-html
	will create them if missing.
	* cron/spiderbts: Update a tmp database before moving it to its
	final place (this should reduce corruptions in case of disk
	issues).

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-html: Cleanup.
	* dl10n-html: Do not create html/include.
	* dl10n-html: Remove the files created in include/ after usage.

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/spiderinit: Activate English.
	* cron/spiderinit: Add intermediate checkpoint for the Galician
	and Romanian mailing lists.
	* cron/spiderinit: Fix display of error messages.
	* cron/spiderinit: Cleanup.
	* cron/spiderinit: Activate Danish, Slovak, and Swedish
	* cron/spiderinit: Do not create the html directories, dl10n-html
	will create them if missing.
	* cron/spiderinit: Explicitly document that we reuse spiderbts'
	OUTDIR

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Do not parse subject when it could
	not be retrieved (e.g. spam removed from web archive).

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/spiderinit.2006: Removed: use cron/spiderinit with FAST set
	to 1 (default).

2012-06-17  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/spiderinit: Added intermediate checkpoint for the Arabic
	mailing list.
	* cron/spiderinit: Update to the i18n.d.o layout.
	* cron/spiderinit: Increase readability with functions.
	* cron/spiderinit: Do not change the status database in the
	working copy directory.
	* cron/spiderinit: Update a tmp database before moving it to its
	final place (this should reduce corruptions in case of disk
	issues).

2012-06-16  Nicolas François  <nicolas.francois@centraliens.net>

	* cron/gen-material: Avoid hard-coded path to the etc directory.
	* etc/gen-material.cfg: Update to the i18n.d.o layout.
	* cron/gen-material: Remove cleanup of file permissions.

2012-06-16  Nicolas François  <nicolas.francois@centraliens.net>

	* debian/changelog, debian/control: Added dependency to po-denconf
	and libsoap-lite-perl.

2012-06-15  Nicolas François  <nicolas.francois@centraliens.net>

	Added files from Churro (no update to i18n.d.o):
	* etc/gen-material.cfg: Configuration for gen-material
	* cron/gen-material: Generate the unstable or testing materials
	* cron/genpts: Generate the PTS data
	* cron/spiderbts: Parse mailing lists on the web
	* cron/spiderinit: Initialize the per language status database
	* cron/spiderinit.2006: Faster version not starting from the
	creation of mailing lists
	* cron/nmu-update: Generate the NMU radar pages
	* cron/dl10n-txt-testing: Generate text reports for testing.

2012-06-15  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/manpages-rrd.pl: No more mipsel in testing & unstable.

2012-06-15  Nicolas François  <nicolas.francois@centraliens.net>

	* compendia, pootle: removed from dl10n.
	* html, htdocs-static: html/ moved to htdocs-static/

2012-02-12  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-html, doc/dl10n-spider_add-new-language.txt,
	lib/Debian/L10n/Html.pm: simplify the process for adding new
	languages. Rely on the lists from lib/Debian/L10n/Utils.pm.
	* lib/Debian/L10n/Utils.pm: Document the difference between
	%LanguageList and %Language.
	* dl10n-html: Execute for all languages when no language arguments
	are provided.
	* dl10n-spider: Fix synopsis. Language argument is optional.
	* lib/Debian/L10n/BTS.pm, lib/Debian/L10n/Spider.pm: Indicate the
	db name (i.e. language) when reporting BTS issues.

2012-02-12  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/Tar.pm: files with type 2 are links. They are not
	supported, but this might be useful in debug mode to understand
	why files may not be supported (e.g. debian/control in gcc-mingw-w64)

2012-02-12  Nicolas François  <nicolas.francois@centraliens.net>

	* doc/dl10n-spider_add-new-language.txt: Added.

2012-02-11  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Fix footer string.

2012-02-11  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-check: Ignore test files from package perl.

2012-01-18  Christian Perrier  <bubulle@debian.org>

	* dl10n-html, lib/Debian/L10n/Html.pm, lib/Debian/L10n/Utils.pm:
	Enable Italian and Danish.

2011-07-28  Nicolas François  <nicolas.francois@centraliens.net>

	* compendia/msg2utf8: Use the Content-Type field to find the
	charset. isutf8 is not always installed.
	* compendia/msg2utf8: Use msgconv in place to convert the
	encoding. There is no need to mess with the Content-Type field.

2011-07-25  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/manpages-rrd.pl: Remove sparc, alpha, hppa.

2011-07-25  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-nmu: The BTS may not know the maintainer of all packages.

2011-07-25  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-html, lib/Debian/L10n/Html.pm, lib/Debian/L10n/Utils.pm:
	Enable galician.

2011-07-25  Nicolas François  <nicolas.francois@centraliens.net>

	* compendia/l10n.conf: Remove the creation of the POTMPDIR from
	the config file. Just define the parent directory TMPDIR.
	* compendia/gen_compendia: Enable set -e but do not fail with
	createcompendium, provide log instead.
	* compendia/createcompendium: The creation of the POTMPDIR is
	moved here, and is removed with trap.
	* compendia/createcompendium: Since set -e is enabled, we need to
	catch the msgcat failures (and we report them back to the log and
	the parent gen_compendia.

2011-02-20  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/BTS.pm: Added source package to a warning.

2011-02-20  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-txt: When there are reports for multiple languages,
	provide a footer only at the end.

2011-02-19  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-html: Added Czech support.

2011-02-19  Nicolas François  <nicolas.francois@centraliens.net>

	* compendia/createcompendium: Remove old compendia and log file
	when a compendia is successfully generated.
	* compendia/convert_or_remove: Really perform cleanup.
	* compendia/l10n.conf: Create the gen-compendia temporary file if
	it does not exist.

2010-09-09  Denis Barbier  <bouzim@gmail.com>

	* lib/Debian/Pkg/Tar.pm: Fix PO extraction bug (dokuwiki)
	affected.

2010-06-09  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/DebSrc.pm: Avoid duplicate report for files
	present in multiple archives.

2010-06-09  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/DebSrc.pm, lib/Debian/Pkg/Tar.pm: A file can be
	included in multiple archives. The archives are parsed in reverse
	order, and once extracted files do not need to be overridden.

2010-06-09  Nicolas François  <nicolas.francois@centraliens.net>

	* html/pseudo-urls.html: There are no bug number with the DONE
	pseudo-tag.

2010-04-12  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/Diff.pm: Allow diff header to end with
	spaces/tabulation. This may break parsing of patches for files
	ending with a space or tabulation.

2010-04-12  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/BTS.pm: Do not complain if a bug is reported
	against a binary package and the source package is in the
	database.

2010-01-03  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/Tar.pm: The leading directory should not be '.',
	include the next directory in that case. This fixes the handling
	of ocfs2-tools, but needs to be checked with the whole archive.

2010-01-03  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-check: Forward the --debug option to the Tar package.

2010-01-03  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Fixed typo, strftime needs an array, not a reference.

2010-01-02  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/manpages-rrd.pl: Updated list of architectures per
	distributions.

2010-01-01  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/dl10n-rrd: Do not warn when a language code is no more
	used.

2010-01-01  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Added generation date and database version
	* dl10n-pts: Added description of errors.

2010-01-01  Nicolas François  <nicolas.francois@centraliens.net>

	* MANIFEST: Updated list of files.
	* lib/Debian/L10n/Utils.pm, lib/Debian/L10n/BTS.pm: Added minimal
	documentation header.
	* Makefile.PL: Updated list of scripts.

2009-12-29  Nicolas François  <nicolas.francois@centraliens.net>

	* compendia/l10n.conf: Directly use the extracted files.
	* compendia/update_fs: There is no need anymore to cleanup the
	previous runs and extract the gziped PO files

2009-12-29  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-check: Only warn about packages already in the database if
	verbose is enabled (different versions do not necessarily generate
	a warning anymore).

2009-12-29  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/Tar.pm: Added option prepend_dir to force
	prepending the directory during parse(). This is needed for the v3
	source package support.
	* lib/Debian/Pkg/DebSrc.pm: Use this option, otherwise the files
	are extracted without the directory.

2009-12-29  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/dl10n-rrd: Do not die when the statistics are empty.
	It occurs for language codes that are no more used.

2009-12-29  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/DebSrc.pm: Hack to support v3 source packages.
	* dl10n-check: In the v3 source packages, the debian archive is
	not rooted to /.

2009-12-29  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/Tar.pm: Fixed typo (unknozn).

2009-09-10  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-check: Added blacklist of PO files (used in testsuites)
	and whitelist of po4a files.

2009-09-10  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Locale/Language.pm: Added languages: an, ang, bem, crh, csb,
	frp, hne, io, kab, mai, mal, mus, nds, new, pms, tlh.
	* lib/Locale/Language.pm: Javanese is jv, not jw.

2009-09-10  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Do not hardcore a todo for shadow.
	* dl10n-pts: Fix mis-detection of todos.

2009-08-28  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-check: Removed non-US sections, and non-US handling.
	* dl10n-check: Updated list of sections.

2009-08-15  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Locale/Language.pm: Added Asturian.
	* dl10n-check: Added support for 3 letters language
	* lib/Debian/L10n/Html.pm: Removed unneeded modules.
	* dl10n-check: Move PO files in 'doc' or 'man' directories in the
	po4a section.

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Escape the '<' and '>' characters in HTML output.

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Fixed broken links.

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Added tests for packages with no up-to-date
	po-debconf translation and less than 5 po-debconf translations.

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Only display the categories where a PO file exist.

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Provide some help to the maintainer on how they can
	check the PO files and what they should do.

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Add the generation time.
	* dl10n-pts: Sort the packages.

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Differentiate between no translations (-) and no
	translated strings (0).

2009-07-23  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Added section regarding errors in translations.

2009-07-22  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Added boolean to indicate if a todo message should be
	displayed on the PTS. Only shadow currently.

2009-07-22  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Fixed URL. i18n is still debian.net.

2009-07-22  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-nmu: Some buggy bugs are sent without a subject. Just
	ignore them when parsing the wnpp and ftp.debian.org bugs.

2009-07-22  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts: Generate more fancy pages.

2009-07-22  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Utils.pm: Do not recode with a broken encoding.

2009-07-22  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/Diff.pm: Fix support for files with a spece in
	their name.

2009-07-22  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-pts, html/translated.png, html/fuzzy.png,
	html/untranslated.png: Added script to generate translation
	statistics for the PTS.

2009-04-19  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/BTS.pm (parse_submitter): Extract the submitter
	before encoding entities. This caused failures in Mail::Address
	(this was triggered by #524358)

2009-03-07  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/BTS.pm: Do not check the BTS if no bugs have to
	be checked.

2009-03-07  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/dl10n-rrd: Add support for status database with
	history. Do not stop on the first status line matching the
	package/part.

2009-03-01  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-html, lib/Debian/L10n/Html.pm, lib/Debian/L10n/Utils.pm:
	Added support for Russian.

2009-02-28  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/example/resize_rrd.sh: Added script to resize the rrd
	databases.

2009-02-21  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/manpages-rrd.pl: Updated list of Architecture for each
	suite.

2009-02-21  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-mail, lib/Debian/L10n/Mail.pm: Pass command line options
	to Mail::process.

2009-02-21  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/Tar.pm: "Some tar files have no trailing null
	block". Added an argument to _io_read to avoid failing when it
	cannot read this bloc at the end of the archive.
	* lib/Debian/Pkg/Tar.pm: Added support for wrong checksums from
	SunOS and HP-UX tar.
	* lib/Debian/Pkg/Tar.pm: Do not warn for the "unable to determine
	top-level directory" errors. This is handled correctly later. The
	warning will be output in debug mode.

2009-01-15  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/BTS.pm, lib/Debian/L10n/Spider.pm: Do not
	overload the BTS soap interface. Prepare the list of bugs we need
	to check, and then check all the bugs at the same time. Thanks to
	Don Armstrong. check_bts_bug_soap() renamed check_bts_bugs_soap().
	The function does not return any status anymore.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* pootle/sync-projects.d/20di: Execute with "set -e".
	* pootle/sync-projects.d/20di: Quote variables.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* pootle/sync-projects.d/10debconf: Execute with "set -e".
	* pootle/sync-projects.d/10debconf: Quote variables.
	* pootle/sync-projects.d/10debconf: Create the temporary directory
	with mktemp instead of tempfile. This avoid having to remove the
	file and create the directory later.
	* pootle/sync-projects.d/10debconf: Better handling of packages
	without a debian/po directory in the root.
	* pootle/sync-projects.d/10debconf: Ignore the errors from msgcat.
	(A lot of empty PO files were created with no valid charset)
	* pootle/sync-projects.d/10debconf: Fix typo: lang -> $lang.
	* pootle/sync-projects.d/10debconf: Use
	/srv/pootle.debian.net/tmp/ for the temporary files and
	directories.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* pootle/sync-projects.d/x20ddtp: Execute with "set -e".
	* pootle/sync-projects.d/x20ddtp: Quote variables.
	* pootle/sync-projects.d/x20ddtp: Removed TEMPDIR variable (not
	used).
	* pootle/sync-projects.d/x20ddtp: Use /srv/pootle.debian.net/tmp/
	for the temporary files.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* pootle/sync-projects.d/30sync: Execute with "set -e".

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* pootle/sync-projects.d/cfg/10debconf,
	pootle/sync-projects.d/cfg/common: Configuration files are just
	sourced. They do not need to be executable and do not need a
	shebang.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/manpages-rrd.pl: Fix wrong detection of system()
	failures. Improve error messages.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: check_bts_bug_soap() is now in the
	Debian::L10n::BTS module.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-check: First check for PO files, then check for nls
	directories. This fix an issue with sysstat, which has PO files in
	a directory named nls.

2008-12-06  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/BTS.pm: Only check the BTS if a status has the
	BTS tag, and no other status line follow for the pkg, type, file.
	This will reduce the load on the BTS and reduce the number of
	mails for the server admins.
	* lib/Debian/L10n/BTS.pm: re-indent.

2008-11-30  Nicolas François  <nicolas.francois@centraliens.net>

	* pootle/sync-projects.d/cfg/10debconf: Fix the link to the
	material tarball. Explicitly point to the unstable material.

2008-11-30  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/example/update_unstable.sh: Fix the location of the
	$dist.gz material statistics file.

2008-11-30  Christian Perrier  <bubulle@debian.org>

	* dl10n-rrd/example/update_unstable.sh: Add missing [ ] in a if
	test.

2008-11-29  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-nmu, dl10n-rrd/example/update_unstableBTS.sh,
	dl10n-rrd/example/update_testing.sh,
	dl10n-rrd/example/update_unstable.sh, dl10n-rrd/example/config.sh,
	compendia/update_fs, compendia/l10n.conf, compendia/gen_compendia:
	Update the file locations according to the location on Churro.

2008-11-29  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Utils.pm: Export the %Status, %Status_syn,
	%Type_syn, %LanguageList, and %Language hashes.

2008-11-29  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm, lib/Debian/L10n/Utils.pm: %Status,
	%Status_syn, %Type_syn, %LanguageList, %Language, parse_subject,
	parse_from, and parse_date moved from Spider to Utils.
	* lib/Debian/L10n/Spider.pm, lib/Debian/L10n/BTS.pm:
	parse_submitter, check_bts, check_bts_soap, and check_bts_bug_soap
	moved from Spider to BTS.
	* lib/Debian/L10n/Spider.pm, lib/Debian/L10n/Db.pm: clean_db moved
	from Spider to Db.
	* lib/Debian/L10n/Db.pm: Added support for the Message-ID header.
	This indicate the last imported mail and replace the
	year/month/message headers when Mail is used instead of Spider.
	* lib/Debian/L10n/Mail.pm, dl10n-mail: New tool intended to
	replace dl10n-spider. This tool can be used to receive mail from
	an mbox or from stdin (procmail filter).
	* lib/Debian/L10n/Utils.pm: parse_from: better handling of MIME
	encoded from lines.
	* lib/Debian/L10n/BTS.pm: check_bts_soap as I'm receiving lots of
	timeout from soap, add a possibility to write the database every
	10 analyzed bugs.

2008-11-28  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/manpages-rrd.pl: No more m68k in unstable?

2008-11-27  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/manpages-rrd.pl: Fail in a better way if the Content
	files could not be downloaded.

2008-11-27  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/example/update_unstable.sh: Make sure the database is
	present and fail in a better way if not.

2008-11-27  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/example/config.sh: Point DL10N_HOME to SVN. Only
	meaningful for the default installation on Churro.

2008-10-28  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm, lib/Debian/L10n/Html.pm, dl10n-html:
	Added support for the Swedish language.

2008-10-25  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Bugs reported to wnpp do not match
	the database's package name.

2008-10-03  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-nmu: Change the page title to "Debian localization radar".

2008-09-07  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-txt: Add support for databases with history.

2008-09-07  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Do not fail if there is no From:
	field. (see
	https://lists.debian.org/debian-l10n-french/2008/09/msg00138.html)

2008-09-07  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Html.pm: Add (uncomment) the anchors for
	translators, types, status, and packages.

2008-08-15  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-html: Fix typo: syntax_message -> syntax_msg.

2008-08-12  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/Pkg/DebSrc.pm: Fix typo in comment.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Html.pm: Added support for database with
	history. Generate the history in the coordination pages for
	by_package, by_type (not for by_date, by_translator, by_status,
	by_bug). Compared to the previous version, the tables look more
	similar from one sorting method to the other. More lines are
	displayed (except for the by_bug pages).

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Define the SOAP object global. This
	removes one argument from check_bts_bug_soap().
	* lib/Debian/L10n/Spider.pm: check_bts_bug_soap() does not remove
	status lines, there is no need re-read the list of status lines.
	* lib/Debian/L10n/Spider.pm: Make sure $soap_bugs is defined
	before checking its length.
	* lib/Debian/L10n/Spider.pm: Added variable for the BTS location.
	* lib/Debian/L10n/Spider.pm (clean_db): Re-read the list of status
	lines for a package after each removal.
	* lib/Debian/L10n/Spider.pm (clean_db): Indicate the most
	recent statusline which can be removed for a file/type. This avoid
	that closure of old bugs remove the status for more recent
	review cycles.
	* lib/Debian/L10n/Spider.pm (clean_db): Log "Remove DONE"
	instead of "Remove bug" when a old DONE is removed.
	* lib/Debian/L10n/Spider.pm: Only check the BTS status if
	$check_bts.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Db.pm: if a $type, $file, and $statusline are
	specified, remove the lines older than the specified $statusline
	for the given $file and $type
	* lib/Debian/L10n/Db.pm: del_status() always uses $statusline.
	Remove the specified line only if $file and $type are not defined.

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* html/l10n.css: Added support for TODO (TAF are still supported).

2008-08-11  Nicolas François  <nicolas.francois@centraliens.net>

	* dl10n-rrd/example/update_unstableBTS.sh,
	dl10n-rrd/example/update_testing.sh,
	dl10n-rrd/example/update_unstable.sh, dl10n-rrd/example/config.sh:
	Updatedto match the pathes on i18n.debian.net (/org -> /srv)
	* dl10n-rrd/example/update_unstableBTS.sh,
	dl10n-rrd/example/update_testing.sh,
	dl10n-rrd/example/update_unstable.sh: Added Galician for
	graph_ranks.

2008-08-04  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Only try to cleanup the package if
	the package actually exists.
	* lib/Debian/L10n/Spider.pm: Make sure the package exists in the
	database before adding a status line.

2008-08-04  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Add the package name to the debug
	information.

2008-08-04  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: When we receive a BTS status, check
	the status of the bug so that we can set it to DONE. This permits
	to clear the history later on if another review cycle is started
	before we notice that the bug is actually closed.

2008-08-04  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Split check_bts_soap() into
	check_bts_soap() and check_bts_bug_soap(). The later permits to
	check the status of a single bug.

2008-08-04  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: After looking at the BTS, only update
	the database if there were some changes. Differentiate changes
	which require adding a status line and the one which only produce
	an update (date & name fixes).

2008-08-04  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: There has beenno issues so far with
	the soap interface. Remove the support for the LDAP BTS interface.

2008-08-04  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: When a new cycle starts (after a
	DONE), do not wait 3 days to erase the old cycle history if a new
	status must be added to the database.

2008-08-03  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Html.pm, lib/Debian/L10n/Spider.pm, dl10n-html:
	s/portuguese_BRAZIL/brazilian/

2008-08-03  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Keep history in the database. Use
	add_status(), instead of set_status(), except when a statusline is
	just fixed (name, date updated).

2008-08-03  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Db.pm (del_status): Add support for clearing a
	specific line in the database specified by $pkg, $type, and $file.
	* lib/Debian/L10n/Db.pm (set_status): Add support to change the
	status of a specific line in the database specified by a
	statusline.

2008-08-03  Nicolas François  <nicolas.francois@centraliens.net>

	* html/pseudo-urls.html: l10n.css is in html/, not ../

2008-08-03  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: Added Romanian to the list of teams.
	* dl10n-html: Likewise.
	* lib/Debian/L10n/Html.pm: Likewise.

2008-08-03  Nicolas François  <nicolas.francois@centraliens.net>

	* lib/Debian/L10n/Spider.pm: TAF is now deprecated. Please use
	TODO.
	* lib/Debian/L10n/Spider.pm: TAF is an alias for TODO.

2008-08-03  Nicolas François  <nicolas.francois@centraliens.net>

	* Changelog: Added Changelog file. Previous changes are documented
	in commits, or in debian/changelog.

