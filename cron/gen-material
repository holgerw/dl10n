#! /bin/sh 

set -e

PROGNAME=$(basename $0)

# Get global config and checks
. $(dirname $(readlink -f $0))/../etc/dl10n.conf

d=$1
shift

if [ -z "$d" ]
then
    echo "Usage: $0 <distribution>"
    exit 1
fi

test -d $OUTDIR/data || mkdir -p $OUTDIR/data

# Log file entry (distribution and time)
echo "Running gen-material for $d at $(date -u) ..." >> $LOGPREFIX.log

# To restrict the package names grep-dctrl -n -s Directory,Files -P -e '^[a-c]'
#   WARNING: grep-dctrl seems to be different in woody and sarge
#   WARNING: one or two N are needed!
gzip -dc $MIRRORDIR/dists/$d/main/source/Sources.gz \
         $MIRRORDIR/dists/$d/contrib/source/Sources.gz \
         $MIRRORDIR/dists/$d/non-free/source/Sources.gz \
    | grep-dctrl -n -s Directory,Files '' \
    | sed -n -e '/^pool\//{N;N;s/\n.* /\//g;p;}' \
    | sed -e "s,^,$MIRRORDIR/," \
    | grep -Ev "/${IGNMATERIAL}/" \
    | $DL10NDIR/dl10n-check \
          --remove-unused \
          --files-from=- \
          --tmp=$TMPDIR \
          --db=$OUTDIR/data/$d \
          --po=$OUTDIR/po/$d \
          --templates=$OUTDIR/templates/$d \
          --menu=$OUTDIR/menu/$d $* >> $LOGPREFIX.log 2>> $LOGPREFIX.err
          
gzip -f $OUTDIR/data/$d

# Only display the errors to the cronjob subscribers
[ -f $LOGPREFIX.err ] && cat $LOGPREFIX.err
