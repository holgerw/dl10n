#!/bin/sh
set -e

# update_unstable.sh
#
# Update the RRD database for the unstable suite, and generate the graphs.
# RRD and graphs are also generated for the unstable manpages.
#
# Copyright (C) 2007 Nicolas François
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

PROGNAME=$(basename $0)

# Get global config and checks
. $(dirname $(readlink -f $0))/../etc/dl10n.conf


GRAPH_SCRIPTS_DIR=$(dirname $0)

dist=$1

OPTS=""
export LC_ALL=C

cd $OUTDIR/${dist}/
if [ ! -e $MATERIALDIR/data/$dist.gz ]
then
    echo "No $MATERIALDIR/data/$dist.gz! Exiting."
    exit 1
fi
gunzip -c $MATERIALDIR/data/$dist.gz > $dist

if [ -n "$2" ]
then
	if [ "$2" = "BTS" ]
	then
		OPTS="--assume-bts"
	fi
fi

PERLLIB=$DL10NDIR/lib $DL10NDIR/dl10n-rrd/dl10n-rrd $OPTS --db=${dist}$2

for period in week month year
do
    if [ ! -d "$period" ]
    then
        mkdir "$period"
    fi
    for fmt in po po4a podebconf
    do
# Hardcoded list of languages, based on the current ranks (2007 03 03)
        range=""
        case $fmt in
            po)
                languages=${RRD_LANGS_PO} ;;
            podebconf)
                range="--upper-limit 11000 --lower-limit 5000 --rigid";
                languages=${RRD_LANGS_PODEBCONF} ;;
            po4a)
                languages=${RRD_LANGS_PO4A} ;;
        esac
        $DL10NDIR/dl10n-rrd/graph_ranks.sh \
            -o "$period/$fmt.png" \
            "$fmt" \
            $languages \
            -- \
            --zoom 1.5 --start end-1$period --end 00:00+1d \
            $range

        case $fmt in
            podebconf)
                range="--upper-limit 11000 --lower-limit 0 --rigid";;
        esac
        for lang in $fmt/*.rrd
        do
            lang=$(basename $lang)
            lang=${lang%.rrd}
            $DL10NDIR/dl10n-rrd/graph_lang.sh \
                -o "$period/$fmt-$lang.png" \
                "$fmt" \
                "$lang" \
                $range \
                --start end-1$period --end 00:00+1d
        done
    done
done

rm -f $dist

#
# man pages
#
if [ -z "$2" ]
then
	$DL10NDIR/dl10n-rrd/manpages-rrd.pl $dist

	$DL10NDIR/dl10n-rrd/manpages_graph_ranks.sh \
	    ${RRD_LANGS_MANPAGES} \
	    -- \
	    --zoom 1.5 \
	    --start end-1month --end 00:00+1d
fi
