#!/bin/bash
set -e

# graph_ranks.sh
#
# Create a graph representing the variations of translated strings for the
# given format and the given languages
#
# Copyright (C) 2007 Nicolas François
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

if [ $# -lt 2 ]
then
        echo "Usage: $0 [-o file] <format> <languages> [-- rrdtool_options]" >&2
        echo "formats: po, podebconf, po4a"  >&2
        echo "rrdtool_options defaults to $rrdtool_options" >&2
        exit 1
fi

. $(dirname $0)/colors.sh

rrdtool_options="--start now-2d"

if [ "$1" = "-o" ]
then
    output="$2"
    shift 2
fi

fmt=$1
shift

if [ -z "$output" ]
then
    output="$fmt.png"
fi

languages=""
n=0

while true
do
    arg=$1
    if [ -z "$arg" ]
    then
        # End of languages & rrdtool options
        break
    else
        if [ "$arg" = "--" ]
        then
            # Begining of the rrdtool options
            shift
            rrdtool_options=$@
            break
        else
            languages="$languages $arg"
            n=$((n+1))
        fi
    fi

    # Next argument
    shift
done

export LC_ALL=C

a=0

#echo -n "Generating $fmt.png... "
rrdtool graph "$output" \
                 --units-exponent 0 \
                 --height 200 \
                 --title "$fmt strings" \
                 --vertical-label "strings" \
                 $rrdtool_options \
                 DEF:total=$fmt/__.rrd:u:AVERAGE \
                 VDEF:vdeftotal=total,LAST \
                 $(for l in $languages; do
                     echo -n " DEF:$l=$fmt/$l.rrd:t:AVERAGE"
                     echo -n " VDEF:vdef$l=$l,LAST"
                   done) \
                 LINE:total#000000:total \
                 GPRINT:vdeftotal:%6.0lf\\l \
                 $(for l in $languages; do
                     echo -n " LINE:$l#$(get_color $n $a):$l"
                     echo -n " GPRINT:vdef$l:%6.0lf"
                     [ "$(( a % 4 ))" = "3" ] && echo -n "\l"
                     a=$((a+1))
                   done) \
                 COMMENT:\\s \
                 COMMENT:\\s \
                 COMMENT:"$(date|sed 's/:/\\:/g')" \
                 COMMENT:"Comments\: debian-l10n-devel@lists.alioth.debian.org" \
                 > /dev/null
#echo "done."

